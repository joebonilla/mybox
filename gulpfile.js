var elixir = require('laravel-elixir');
elixir.config.autoprefix = true;
elixir.config.production = true;
elixir.config.sourcemaps = true;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    mix
        .copy('bower_components/bootstrap/less', 'resources/assets/bootstrap')
        .copy('bower_components/bootstrap/fonts', 'public/fonts/')
        .copy('bower_components/fontawesome/fonts', 'public/fonts/')
        .copy('bower_components/fontawesome/less', 'resources/assets/fontawesome')
        .copy('bower_components/bootstrap/dist/js/bootstrap.min.js', 'resources/assets/js')
        .copy('bower_components/jquery/dist/jquery.min.js', 'resources/assets/js')
        .scripts([
            'jquery.min.js', 'bootstrap.min.js'
        ])
        .less('app.less')
        .version(['css/app.css','js/all.js']);
});
